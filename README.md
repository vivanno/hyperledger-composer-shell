# hyperledger-composer-shell

## get 

* docker pull vivanno/hyperledger-composer-shell

## usage

### interactive shell

* docker run --rm -ti vivanno/hyperledger-composer-shell bash 

### command 

* docker run --rm -ti vivanno/hyperledger-composer-shell bash -c "source ~/.bashrc && composer card list"

## complete readme?

see: https://hub.docker.com/r/vivanno/hyperledger-composer-shell/